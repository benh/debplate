DEBPLATE := debplate

debian/control debian/tests/control: debian/debplate-config.yml $(wildcard debian/templates/*.control debian/templates/*.tests)
	$(DEBPLATE) source $(CURDIR)

debian/rules.gen: debian/debplate-config.yml $(wildcard debian/templates/*.rules)
	$(DEBPLATE) build $(CURDIR)

ifneq ($(filter-out debian/control debian/test/control %clean,$(MAKECMDGOALS)),)

# Define variables/macros for characters that are special in makefiles.
# These will be used in debian/rules.gen if necessary.
debplate_empty :=
debplate_ht := $(debplate_empty)	$(debplate_empty)
define debplate_lf


endef
debplate_sp := $(debplate_empty) $(debplate_empty)
define debplate_hash
#
endef

include debian/rules.gen

endif

debplate_clean:
	$(DEBPLATE) clean $(CURDIR)

.PHONY: debplate_clean
