# debplate

A template system for Debian packages.

It generates:

* `debian/control` and (optionally) `debian/tests/control`
* A makefile, `debian/rules.gen`, to be included in `debian/rules`
* Arbitrary per-package files in the `debian` directory

The configuration allows for a hierarchical grouping of binary
packages, where each group can contain any number of binary packages
and child groups.

## Contact

The project is currently maintained at
<https://salsa.debian.org/benh/debplate>.  Please open issues and
merge requests there.

## Configuration file

The debplate configuration for a source package is specified by
`debian/debplate-config.yml`, in YAML syntax.  The schema of this file
is currently subject to change, but is expected to be stabilised in
the near future.

### Group definitions

Each group definition is a mapping, with the following keys defined:

* `architecture`: (optional) An architecture name, or names, which
  this group is restricted to.

  This can be used to define the Architecture field for all packages
  in the group instead of specifying it in each package's control file
  template.

  This is inherited by child groups.  It can be overridden, but the
  new value should normally be a subset of the inherited value.

* `build-profiles`: (optional) An expression specifying build
  profile(s) which this group is restricted to.

  This can be used to define the Build-Profiles field for all package
  in the group instead of specifying it in each package's control
  file template.

  This is inherited by child groups.  It can be overridden, but the
  new value should normally be more restrictive than the inherited
  value.

* `enabled`: (optional) Boolean flag indicating whether the group is
  enabled.

  The default is `yes`.  If it is set to `no`, the group will not be
  processed, and this cannot be overridden by child groups.

* `variables`: (optional) Mapping of variable names to values.

  Variable definitions are inherited by child groups, but can be
  overridden.

* `templates`: (optional) List of templates to be processed.

  Each entry is the name of a collection of templates, or of a
  separate `.rules` or `.tests` template.

* `templates_level`*n*: (optional) List of templates to be processed
  within a descendant group *n* levels down.

  This can be used to avoid the need to repeat a list of templates in
  multiple child groups.  A child group can override it by specifying
  `templates` itself.

* `groups`: (optional) Mapping of child group names to their
  definitions.

The top level of the file is a group definition with some additional
keys defined:

* `version`: (required) Version of the configuration schema being
  used.  Currently this must be 0.

* `include-control`: (optional) External command(s) whose output should
  be included in `debian/control`.  This is a sequence of mappings,
  each with the key `command`.

  The command must produce valid binary package paragraphs whose
  names do not overlap with those produced by templates.

### Variable definitions

Variable names normally consist of word characters, since it will
not be possible to refer to them from a template otherwise.

Two special name forms are currently supported:

* *name*`.extend`: Define *name* to be the inherited value
  concatenated with the value given here.  Both values must be
  YAML sequences.

* *name*`.file`: Define *name* to be the contents of the named
  file.  It must be a UTF-8 (or ASCII) encoded text file.

Several variables are predefined by debplate:

* `changelog`: Object representing the top changelog entry.
  It has the attributes `author`, `date`, `debian_revision`,
  `debian_version`, `distributions`, `epoch`, `full_version`,
  `package`, `urgency`, `version`, which are as defined in
  the [debian.changelog.Changelog][python-debian-changelog]
  class.

* `group`: Name of the group.

* `child_groups`: List of child group names.

* `package`: Name of the binary package.  This is not defined when
  processing a `.control` template, or a separate `.rules` or `.tests`
  template.

## Template files

Template files are written using [Jinja syntax][jinja-templates],
which allows expressions and control statements to be embedded within
whatever syntax the output file uses.

Many additional filter functions are supported,
[taken from Ansible][ansible-filters].

### File naming

All template files must be placed in the `debian/templates` directory,
and be named as follows:

| Template extension | Output file(s)             |
| ------------------ | -------------------------- |
| `.control`         | `debian/control`           |
| `.tests`           | `debian/tests/control `    |
| `.rules`           | `debian/rules.gen `        |
| `.`*ext*           | `debian/`*package*`.`*ext* |

The source package paragraph in `debian/control` is always produced
from a template named `source.control`, which should not be mentioned
in the configuration.

The templates for a binary package's `debian/control` paragraph and
all per-package files must have the same basename.  For example,
a group might include a shared library package with the templates
`library.control`, `library.postinst`, and `library.shlibs`.  This
collection of templates would be specified as `library`.

Since a makefile rule or test definition can relate to multiple binary
packages, templates of these types can either be included in such a
collection or be specified separately.

### Processing of `.control` templates

Each `.control` template must produce a single paragraph each time it
is processed.  (Currently additional paragraphs are quietly dropped,
but in future they will be treated as an error.)

When an expression in a template produces a line break, space and `.`
characters are automatically added to produce valid continuation
lines in the output.

Some control fields in binary package paragraphs are handled
internally by debplate and not copied to the output file:

* **Build-Depends**: Lists build-dependencies required only if this
  binary package will be built.  Each dependency is appended to the
  source package's Build-Depends field.  If the binary package is only
  built on some architectures or in some build profiles, the
  dependency is restricted accordingly.

* **X-Test-Group-Dependency**: Lists test groups (defined in the next
  section) for which this binary package is a dependency.

### Processing of `.tests` templates

Similarly to `.control` templates, each `.tests` template must produce
a single paragraph, referred to here as a *test group*.  Expressions
that produce line breaks are handled in the same way as in `.control`
templates.

A test group may have a name.  This is defined either by an
X-Test-Group field (which is not copied to the output file) or by a
Tests field that contains a single test name.

If the test group has a name, any binary packages that listed that
name in their X-Test-Group-Dependency field are appended to the
Depends field of the test group.  Where a binary package is only built
on some architectures, the dependency is restricted to those
architectures.

### Processing of `.rules` templates

When an expression produces characters that are special in make
syntax, they are automatically escaped.  Characters that are special
in shell syntax are *not* escaped; use the `quote` filter to do that.

### Processing of other templates

When processing a template with an extension of `.bug-control`,
`.doc-base`, `.doc-base.*` or `.templates`, expressions that produce
line breaks are handled in the same way as in `.control` templates.

If the output of the template contains only white-space, no output
file is created.  This can be used to make the creation of an
output file conditional.

The permission bits of the template file are copied to the output
file.  This can be used to ensure that an output file is executable.

## Makefiles

debplate should normally be invoked through the makefiles that it
installs:

* `/usr/share/debplate/common.mk`: This defines rules to build
  `debian/control`, `debian/tests/control` and `debian/rules.gen`.  It
  includes `debian/rules.gen` when necessary.  It also defines the
  `debplate_clean` target which will run `debplate clean`.

  Source packages *not* using dh should include this in
  `debian/rules`, and should make their `clean` target depend on
  `debplate_clean`.

* `/usr/share/debplate/dh.mk`: This includes `common.mk`, and defines
  an `execute_after_dh_auto_clean` [double-colon
  rule][make-double-colon] depending on `debplate_clean`.

  Source packages using dh should include this in
  `debian/rules`.  If they define an `execute_after_dh_auto_clean`
  rule, it must be a double-colon rule.

## Command syntax

Build the `debian/control` and `debian/tests/control` files,
which must be present in the source package:

> `debplate source` *source-directory*

Build all other files for the current host architecture:

> `debplate build` *source-directory*

Delete all other files:

> `debplate clean` *source-directory*

[ansible-filters]: https://docs.ansible.com/ansible/latest/user_guide/playbooks_filters.html
[jinja-templates]: https://jinja.palletsprojects.com/templates/
[make-double-colon]: https://www.gnu.org/software/make/manual/html_node/Double_002dColon.html
[python-debian-changelog]: https://python-debian-team.pages.debian.net/python-debian/api/debian.changelog.html#debian.changelog.Changelog
