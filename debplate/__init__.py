import fnmatch
import itertools
import os.path
import re
import subprocess

import debian.changelog
import debian.deb822
import jinja2
import jinja_vanish
import jsonschema
import yaml

from .filters import all_filters


_line_re = re.compile(r'(.*)\n')


@jinja_vanish.markup_escape_func
def deb822_escape(val):
    s = str(val)
    return _line_re.sub(
        lambda match: ((match.group(1) or '.') + '\n'
                       + (' ' if match.end != len(s) else '')),
        s)


_makefile_special_re = re.compile(r'(^[ \t]|[ \t\\]$|[\n#$])')


@jinja_vanish.markup_escape_func
def makefile_escape(val):
    s = str(val)
    repl = {
        '\t': '$(debplate_ht)',
        '\n': '$(debplate_lf)',
        ' ':  '$(debplate_sp)',
        '#':  '$(debplate_hash)',
        '$':  '$$',
        '\\': '\\$(debplate_empty)',
    }
    return _makefile_special_re.sub(lambda match: repl[match.group(1)], s)


class ChangelogData:
    def __init__(self, package_dir):
        with open(os.path.join(package_dir, 'debian/changelog')) as f:
            changelog = debian.changelog.Changelog(f, max_blocks=1)

        # Copy most attributes from the changelog block, but not
        # 'changes' as that might force debian/control to be
        # regenerated very often, and not 'versions' since we only
        # read the top version.
        for name in ['author', 'date', 'debian_revision', 'debian_version',
                     'distributions', 'epoch', 'full_version', 'package',
                     'urgency', 'version']:
            setattr(self, name, getattr(changelog, name))


_schema_v0_group_properties = {
    'architecture': {
        'type': 'string'
    },
    'build-profiles': {
        'type': 'string'
    },
    'enabled': {
        'type': 'boolean'
    },
    'variables': {
        'type':                 'object',
        # Keys must be valid variable names, optionally
        # follwed by .extend or .file
        'patternProperties': {
            r'^[A-Za-z_][0-9A-Za-z_]*(\.(extend|file))?$': {
                # Values can be anything
            }
        },
        'additionalProperties': False,
    },
    'templates': {
        '$ref': '#/definitions/template_list'
    },
    'groups': {
        'type':                 'object',
        # Keys are arbitrary strings; values are child groups
        'patternProperties':    {'.': {'$ref': '#/definitions/group'}},
    },
}


_schema_v0_group_pattern_properties = {
    '^templates_level[0-9]+$': {'$ref': '#/definitions/template_list'},
}


_schema_v0 = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'title':   'debplate top-level configuration',
    'definitions': {
        'filename': {
            'type':    'string',
            'pattern': r'^[^/]+$',
        },
        'template_list': {
            'anyOf': [
                {
                    'type':        'array',
                    'items':       {'$ref': '#/definitions/filename'},
                    'uniqueItems': True,
                },
                {
                    '$ref': '#/definitions/filename',
                },
            ],
        },
        'group': {
            'type':                 'object',
            'properties':           _schema_v0_group_properties,
            'patternProperties':    _schema_v0_group_pattern_properties,
            'additionalProperties': False,
        }
    },
    'type':                 'object',
    'properties': {
        'version':         {'const': 0},
        'include-control': {
            'type': 'array',
            'items': {
                'type':                 'object',
                'properties': {
                    'command': {'type': 'string'},
                },
                'additionalProperties': False,
                'required':             ['command'],
            },
        },
        **_schema_v0_group_properties,
    },
    'patternProperties':    _schema_v0_group_pattern_properties,
    'additionalProperties': False,
    'required':             ['version']
}


_config_name = 'debian/debplate-config.yml'
_template_subdir_name = 'debian/debplate'
_input_comment_prefix = '# debplate-input: '


class ControlConflictError(Exception):
    pass


class TemplateOutputError(Exception):
    pass


class InvalidSourceError(Exception):
    pass


# This is only meant to catch mistakes, so use MD5 which produces a
# relatively short hash
def _hash_file(filename):
    import hashlib
    with open(filename, 'rb') as f:
        return hashlib.md5(f.read()).digest()


class TemplateProc:
    def __init__(self, package_dir):
        self._package_dir = package_dir
        self._template_dir = os.path.join(package_dir, _template_subdir_name)
        self._template_file_names = \
            [file_name for file_name in os.listdir(self._template_dir)
             if not (file_name.startswith('.') or file_name.endswith('~'))]

        self._deb822_env = jinja_vanish.DynAutoEscapeEnvironment(
            loader=jinja2.FileSystemLoader(self._template_dir),
            autoescape=True,
            escape_func=deb822_escape,
            undefined=jinja2.StrictUndefined)
        self._deb822_env.filters.update(all_filters)
        self._rules_env = jinja_vanish.DynAutoEscapeEnvironment(
            loader=jinja2.FileSystemLoader(self._template_dir),
            autoescape=True,
            escape_func=makefile_escape,
            undefined=jinja2.StrictUndefined)
        self._rules_env.filters.update(all_filters)
        self._other_env = jinja2.Environment(
            loader=jinja2.FileSystemLoader(self._template_dir),
            autoescape=False,
            undefined=jinja2.StrictUndefined)
        self._other_env.filters.update(all_filters)

        with open(os.path.join(package_dir, _config_name)) as f:
            config = yaml.safe_load(f.read())

        jsonschema.validate(config, schema=_schema_v0)

        self._config = config

    def _process_recursive(self):
        init_vars = {
            'changelog': ChangelogData(self._package_dir)
        }
        self._process_config([], init_vars, {}, None, self._config)

    def _process_config(self, ancestors, parent_vars, parent_restrict,
                        group_name, config):
        # Get architecture and build-profile restrictions
        restrict = parent_restrict.copy()
        if 'architecture' in config:
            restrict['Architecture'] = config['architecture']
        if 'build-profiles' in config:
            restrict['Build-Profiles'] = config['build-profiles']

        try:
            temp_names = config['templates']
        except KeyError:
            # At lower levels, templates might be specified by an ancestor
            temp_names = []
            for i in range(1, len(ancestors) + 1):
                attr_name = 'templates_level%d' % i
                try:
                    temp_names = ancestors[-i][attr_name]
                    break
                except KeyError:
                    pass

        # Allow a string if only one template is needed
        if isinstance(temp_names, str):
            temp_names = [temp_names]

        child_groups = {
            child_group_name: child_config
            for child_group_name, child_config
            in config.get('groups', {}).items()
            # Allow any part of the hierarchy to be disabled
            if child_config.get('enabled', True)
        }

        our_vars = parent_vars.copy()
        for key, value in config.get('variables', {}).items():
            key_parts = key.split('.', 1)
            if len(key_parts) == 1:
                pass
            elif key_parts[1] == 'extend':
                parent_value = our_vars.get(key_parts[0])
                if not isinstance(value, list) \
                   or not isinstance(parent_value, list):
                    raise TypeError('%s: cannot extend a %s with a %s'
                                    % (key, type(parent_value).__name__,
                                       type(value).__name__))
                value = parent_value + value
            elif key_parts[1] == 'file':
                with open(os.path.join(self._package_dir, value)) as f:
                    value = f.read()
            else:
                assert False, '%s: unrecognised dotted key name' % key
            our_vars[key_parts[0]] = value
        our_vars['group'] = group_name
        our_vars['child_groups'] = list(child_groups.keys())

        # At root, implicitly include source control template
        if len(ancestors) == 0:
            self._process_control(our_vars, {}, 'source.control', True)

        our_package_names = set()
        rules_file_names = []
        other_file_names = []

        # Process control templates and collect all the other templates
        for temp_name in temp_names:
            our_vars.pop('package', None)

            if temp_name.endswith('.rules'):
                # rules don't have to be associated with a single
                # binary package
                rules_file_names.append((temp_name, None))

            elif temp_name.endswith('.tests'):
                # Same for tests/control
                self._process_test_group(our_vars, temp_name)

            else:
                package = self._process_control(our_vars, restrict,
                                                temp_name + '.control')
                package_name = package['Package']
                our_package_names.add(package_name)
                our_vars['package'] = package_name

                name_glob = temp_name + '.*'
                for temp_file_name in self._template_file_names:
                    if not fnmatch.fnmatch(temp_file_name, name_glob):
                        continue
                    name_ext = temp_file_name[len(temp_name):]
                    if name_ext == '.control':
                        pass
                    elif name_ext == '.rules':
                        rules_file_names.append((temp_file_name, package_name))
                    elif name_ext == '.tests':
                        self._process_test_group(our_vars, temp_file_name)
                    else:
                        other_file_names.append((temp_file_name,
                                                 package_name, name_ext))

        # Process rules templates for enabled binary packages, and
        # rules templates not associated with a single package if any
        # binary package is enabled
        for temp_file_name, package_name in rules_file_names:
            if package_name is not None:
                package_enabled = self._is_package_enabled(package_name)
            else:
                package_enabled = False
                for package_name in our_package_names:
                    if self._is_package_enabled(package_name):
                        package_enabled = True
                        break
            if package_enabled:
                self._process_rules(our_vars, temp_file_name)

        # Process other templates for enabled binary packages
        for temp_file_name, package_name, name_ext in other_file_names:
            if self._is_package_enabled(package_name):
                self._process_other(our_vars, temp_file_name,
                                    package_name, name_ext)

        ancestors.append(config)
        for child_group_name, child_config in child_groups.items():
            self._process_config(ancestors, our_vars, restrict,
                                 child_group_name, child_config)
        ancestors.pop()

    def _render_deb822_single_para(self, our_vars, temp_file_name):
        text = (self._deb822_env.get_template(temp_file_name)
                .render(**our_vars))
        n_para = 0
        for para in debian.deb822.Deb822.iter_paragraphs(text):
            n_para += 1
            if n_para > 1:
                raise TemplateOutputError('%s: produced multiple paragraphs'
                                          % temp_file_name)
        if n_para == 0:
            raise TemplateOutputError('%s: produced no output' % temp_file_name)
        return para

    def _process_control(self, our_vars, restrict, temp_file_name,
                         is_source=False):
        package = self._render_deb822_single_para(our_vars, temp_file_name)
        for key in restrict:
            if key in package:
                raise ControlConflictError(
                    '%s specified in both template and config' % key)
            package[key] = restrict[key]

        self._write_control(package, is_source)

        return package

    def _process_rules(self, our_vars, temp_file_name):
        text = self._rules_env.get_template(temp_file_name).render(**our_vars)
        self._write_rules(text)

    def _process_test_group(self, our_vars, temp_file_name):
        test_group = self._render_deb822_single_para(our_vars, temp_file_name)
        self._write_tests_control(test_group)

    def _process_other(self, our_vars, temp_file_name, package_name, name_ext):
        out_file_name = package_name + name_ext
        if name_ext in ['.bug-control', '.doc-base', '.templates'] \
           or name_ext.startswith('.doc-base.'):
            env = self._deb822_env
        else:
            env = self._other_env
        text = env.get_template(temp_file_name).render(**our_vars)
        self._write_other(temp_file_name, out_file_name, text)


class SourceTemplateProc(TemplateProc):
    def __call__(self):
        self._source_inputs = {}
        self._packages = {}
        self._test_groups_anon = []
        self._test_groups = {}

        self._add_source_input(_config_name)
        self._process_recursive()
        self._process_include_control()

        source_package = self._packages.pop('source')
        for package in self._packages.values():
            self._move_build_depends(source_package, package)
            self._move_test_dependencies(self._test_groups, package)

        with open(os.path.join(self._package_dir, 'debian/control'), 'w') \
             as control_file:
            source_package.dump(control_file, text_mode=True)
            for package in self._packages.values():
                control_file.write('\n')
                package.dump(control_file, text_mode=True)

            # Write out input hashes so we can check debian/control is
            # up-to-date later
            control_file.write('\n')
            for filename in sorted(self._source_inputs.keys()):
                hash = self._source_inputs[filename]
                control_file.write(_input_comment_prefix
                                   + f'{filename} {hash.hex()}\n')

        if self._test_groups_anon or self._test_groups:
            os.makedirs(os.path.join(self._package_dir, 'debian/tests'),
                        exist_ok=True)
            with open(os.path.join(self._package_dir, 'debian/tests/control'),
                      'w') as control_file:
                first = True
                for test_group in itertools.chain(self._test_groups_anon,
                                                  self._test_groups.values()):
                    if not first:
                        control_file.write('\n')
                    test_group.dump(control_file, text_mode=True)
                    first = False

    # Record hash of an input to debian/control and debian/tests/control
    def _add_source_input(self, filename):
        if filename not in self._source_inputs:
            self._source_inputs[filename] = _hash_file(
                os.path.join(self._package_dir, filename))

    def _is_package_enabled(self, package_name):
        return True

    def _process_control(self, our_vars, restrict, temp_file_name,
                         is_source=False):
        self._add_source_input(os.path.join(_template_subdir_name,
                                             temp_file_name))
        return super()._process_control(our_vars, restrict, temp_file_name,
                                        is_source=is_source)

    def _write_control(self, package, is_source):
        package_name = 'source' if is_source else package['Package']
        prev_package = self._packages.get(package_name)
        if prev_package:
            self._merge_into_package(prev_package, package)
        else:
            self._packages[package_name] = package

    def _process_include_control(self):
        included_packages = []

        for item in self._config.get('include-control', []):
            with subprocess.Popen(
                    item['command'], cwd=self._package_dir,
                    shell=True, stdout=subprocess.PIPE, text=True) as proc:
                for package \
                     in debian.deb822.Deb822.iter_paragraphs(proc.stdout):
                    # Check for name collisions
                    package_name = package['Package']
                    if package_name in self._packages:
                        raise ControlConflictError(
                            'package %s: produced by both template'
                            ' and external generator',
                            package_name)
                    included_packages.append(package)

        for package in included_packages:
            package_name = package['Package']
            prev_package = self._packages.get(package_name)
            if prev_package:
                self._merge_into_package(prev_package, package)
            else:
                self._packages[package_name] = package

    @staticmethod
    def _merge_into_package(prev_package, package):
        package_name = package['Package']

        # Check that they both specify Architecture
        if not package.get('Architecture') \
           or not prev_package.get('Architecture'):
            raise ControlConflictError(
                'package %s: generated repeatedly without architectures'
                ' specified'
                % package_name)

        prev_arches = prev_package['Architecture'].split()
        arches = package['Architecture'].split()

        # Check that all other fields are identical
        for key in set(package.keys()) | set(prev_package.keys()):
            if key != 'Architecture' \
               and package.get(key) != prev_package.get(key):
                raise ControlConflictError(
                    'package %s: different values for %s on architectures %s'
                    ' and %s'
                    % (package_name, key, prev_arches[0], arches[0]))

        prev_package['Architecture'] = \
            ' '.join(sorted(set(arches + prev_arches)))

    @staticmethod
    def _move_build_depends(source_package, package):
        # Move Build-Depends from binary package to source package
        from debian.deb822 import PkgRelation

        # debian.deb822 does not provide a public function to do this :-(
        def parse_build_profiles(s):
            relations = PkgRelation.parse_relations('dummy ' + s)
            return relations[0][0]['restrictions']

        depends_str = package.pop('Build-Depends', None)
        if not depends_str:
            return
        depends = PkgRelation.parse_relations(depends_str)

        # Add architecture and/or profile qualifications to all terms
        # that don't have them
        arch_names = package['Architecture']
        if arch_names in ['all', 'any']:
            arches = []
        else:
            arches = [PkgRelation.ArchRestriction(True, arch)
                      for arch in arch_names.split()]
        restrictions = parse_build_profiles(package.get('Build-Profiles', ''))
        for group in depends:
            for term in group:
                if term.get('archs') is None:
                    term['archs'] = arches
                if term.get('restrictions') is None:
                    term['restrictions'] = restrictions
        depends_str = PkgRelation.str(depends)

        # Append to the appropriate field
        # XXX Should avoid duplicates
        if arch_names == 'all':
            relation_name = 'Build-Depends-Indep'
        else:
            relation_name = 'Build-Depends-Arch'
        if relation_name in source_package:
            depends_str = source_package[relation_name] + ', ' + depends_str
        source_package[relation_name] = depends_str

    def _write_rules(self, text):
        pass

    def _process_test_group(self, our_vars, temp_file_name):
        self._add_source_input(os.path.join(_template_subdir_name,
                                             temp_file_name))
        super()._process_test_group(our_vars, temp_file_name)

    def _write_tests_control(self, test_group):
        # Test group name comes from the X-Test-Group field, or failing
        # that the Tests field if it has a single test name
        group_name = test_group.pop('X-Test-Group', None)
        if group_name is None:
            test_names = test_group.get('Tests')
            if test_names is not None and ',' not in test_names:
                group_name = test_names.strip()

        if group_name is None:
            self._test_groups_anon.append(test_group)
        elif group_name in self._test_groups:
            raise ControlConflictError('duplicate definitions of test group %s'
                                       % group_name)
        else:
            self._test_groups[group_name] = test_group

    @staticmethod
    def _move_test_dependencies(test_groups, package):
        # Move (and invert) X-Test-Group-Dependency fields from binary
        # packages to test groups

        groups_str = package.pop('X-Test-Group-Dependency', None)
        if not groups_str:
            return
        group_names = [term.strip() for term in groups_str.split(',')]

        depends_str = package['Package']

        # Add architecture qualifications.  Currently build profiles
        # are not supported here.
        arch_names = package['Architecture']
        if arch_names not in ['all', 'any']:
            depends_str += ' [%s]' % arch_names

        for group_name in group_names:
            if group_name not in test_groups:
                raise ControlConflictError('%s: test group %s does not exist'
                                           % (package['Package'], group_name))

            test_group = test_groups[group_name]
            if 'Depends' in test_group:
                test_group['Depends'] += ', ' + depends_str
            else:
                test_group['Depends'] = depends_str

    def _write_other(self, temp_file_name, out_file_name, text):
        pass


class BuildTemplateProc(TemplateProc):
    def __call__(self):
        self._check_source_inputs()

        self._enabled_package_names = self._get_enabled_package_names()

        with open(os.path.join(self._package_dir, 'debian/rules.gen'), 'w') \
             as rules_file:
            self._rules_file = rules_file
            self._process_recursive()
            self._rules_file = None

    def _check_source_inputs(self):
        with open('debian/control') as f:
            for line in f:
                if not line.startswith(_input_comment_prefix):
                    continue
                filename, old_hash_hex = \
                    line[len(_input_comment_prefix):].rstrip('\n').split()
                new_hash = _hash_file(os.path.join(self._package_dir,
                                                   filename))
                if new_hash != bytes.fromhex(old_hash_hex):
                    raise InvalidSourceError(
                        f'{filename} has changed since debian/control and'
                        f' debian/tests/control were generated')

    def _get_enabled_package_names(self):
        with subprocess.Popen(
                ['dh_listpackages'], cwd=self._package_dir,
                stdout=subprocess.PIPE, text=True) as proc:
            return set(line.rstrip() for line in proc.stdout.readlines())

    def _is_package_enabled(self, package):
        return package in self._enabled_package_names

    def _write_control(self, package, is_source):
        pass

    def _write_rules(self, text):
        self._rules_file.write('\n')
        self._rules_file.write(text)

    def _write_tests_control(self, test_group):
        pass

    def _write_other(self, temp_file_name, out_file_name, text):
        if text != '' and not text.isspace():
            with open(os.path.join(self._package_dir, 'debian',
                                   out_file_name), 'w') as f:
                f.write(text)

                # Also copy permissions (specifically the x bits)
                temp_mode = (os.stat(os.path.join(self._template_dir,
                                                  temp_file_name))
                             .st_mode)
                os.chmod(f.fileno(), temp_mode & 0o777)


class CleanTemplateProc(TemplateProc):
    def __call__(self):
        self._remove_if_exists('rules.gen')
        self._process_recursive()

    def _is_package_enabled(self, package):
        return True

    # We mustn't override _process_control because we still need to
    # process control templates to find package names
    def _write_control(self, package, is_source):
        pass

    def _process_rules(self, our_vars, temp_file_name):
        pass

    def _write_tests_control(self, test_group):
        pass

    def _process_other(self, our_vars, temp_file_name, package_name, name_ext):
        self._remove_if_exists(package_name + name_ext)

    def _remove_if_exists(self, name):
        try:
            os.remove(os.path.join(self._package_dir, 'debian', name))
        except FileNotFoundError:
            pass


def main():
    import sys

    command, package_dir = sys.argv[1:]

    if command == 'source':
        SourceTemplateProc(package_dir)()
    elif command == 'build':
        BuildTemplateProc(package_dir)()
    elif command == 'clean':
        CleanTemplateProc(package_dir)()
    else:
        print('E: unrecognised command: %s' % command, file=sys.stderr)
        sys.exit(2)
