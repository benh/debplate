#!/usr/bin/python3

import email.utils

import debian.changelog
import debian.deb822
from setuptools import setup


with open('debian/changelog') as changelog_file:
    changelog = debian.changelog.Changelog(changelog_file, max_blocks=1)
(author_name, author_email) = email.utils.parseaddr(changelog.author)

with open('debian/control') as control_file:
    control_structure = debian.deb822.Deb822(control_file)
(maintainer_name, maintainer_email) = email.utils.parseaddr(
    control_structure['maintainer'])


setup(name=changelog.package,
      version=str(changelog.version),
      maintainer=maintainer_name,
      maintainer_email=maintainer_email,
      author=author_name,
      author_email=author_email,
      url=control_structure['homepage'],
      license='GPL-3',
      packages=['debplate'],
      entry_points={
          'console_scripts': [
              'execute-debplate = debplate:main',
          ],
      })
